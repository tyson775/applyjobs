//
//  JobDetailViewController.swift
//  ApplyJobs
//
//  Created by Tyson Vignesh on 22/02/18.
//  Copyright © 2018 Tyson. All rights reserved.
//

import UIKit
import SwiftSoup

class JobDetailViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var jobTitleLabel: UILabel!
    @IBOutlet var workerTypeLabel: UILabel!
    @IBOutlet var vacanciesLabel: UILabel!
    @IBOutlet var experienceLabel: UILabel!
    @IBOutlet var maxSalaryLabel: UILabel!
    @IBOutlet var minSalaryLabel: UILabel!
    @IBOutlet var postingDate: UILabel!
    @IBOutlet var jobDescription: UILabel!
    @IBOutlet var jobRequirement: UILabel!
    @IBOutlet var profileButton: UIButton!
    
    var pickerController = UIImagePickerController()
    var selectedImage: UIImage? = nil
    
    @IBOutlet var captureImage: UIButton!
    
    var selectedModel: JobList? = nil
    var detailModel: JobDetails? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let selectedModel = selectedModel {
            getJobDetail(jobID: selectedModel.jobID)
            title = selectedModel.jobTitle
        }
        
        captureImage.layer.borderWidth = 1.0
        captureImage.layer.borderColor = captureImage.titleLabel?.textColor.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            pickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            self.pickerController.sourceType = UIImagePickerControllerSourceType.camera
            pickerController.allowsEditing = true
            self .present(self.pickerController, animated: true, completion: nil)
        }
        else {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            pickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            pickerController.allowsEditing = true
            self.present(pickerController, animated: true, completion: nil)
        }
    }
    
    @IBAction func captureImageAction(_ sender: UIButton) {
        let alertViewController = UIAlertController(title: "", message: "Choose your option", preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default) { (alert) in
            self.openGallary()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
            
        }
        alertViewController.addAction(camera)
        alertViewController.addAction(gallery)
        alertViewController.addAction(cancel)
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        selectedImage = info[UIImagePickerControllerEditedImage] as! UIImage
//        profileButton.setImage(image, for: .normal)
        profileButton.setAttributedTitle(NSAttributedString.init(string: ""), for: .normal)
        profileButton.setImage(selectedImage, for: .normal)
//        profileButton.imageView?.contentMode = .scaleAspectFit
        dismiss(animated:true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Cancel")
        dismiss(animated:true, completion: nil)
    }

    fileprivate func getJobDetail(jobID: String) {
        ProgressHUD.show()
        APIManager.getIndividualJob(jobID: jobID) { (jobDetailModel, errorVal) in
            ProgressHUD.hide()
            if errorVal == nil {
                self.updateView(jobDetailModel: jobDetailModel!)
            }
        }
    }
    @IBAction func applyJobAction(_ sender: Any) {
        if selectedImage == nil {
            let alertViewController = UIAlertController(title: "", message: "Please select capture / choose profile image", preferredStyle: .alert)
            present(alertViewController, animated: true, completion: nil)
            let cancel = UIAlertAction(title: "Ok", style: .cancel) { (alert) in
                self.dismiss(animated:true, completion: nil)
            }
            alertViewController.addAction(cancel)
            return
        }
        if let selectedModel = selectedModel {
            APIManager.applyJob(withJobID: selectedModel.jobID, image: selectedImage!, completion: { (message, error) in
                if error == nil {
                    let alertViewController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                    self.present(alertViewController, animated: true, completion: nil)
                    let cancel = UIAlertAction(title: "Ok", style: .cancel) { (alert) in
                        self.dismiss(animated:true, completion: nil)
                    }
                    alertViewController.addAction(cancel)
                }
            })

        }
    }
    
    fileprivate func updateView(jobDetailModel jobDetail: JobDetails) {
        DispatchQueue.main.async {
            self.jobTitleLabel.text = jobDetail.jobTitle
            self.workerTypeLabel.text = jobDetail.workerType
            self.vacanciesLabel.text = "\(jobDetail.numberOfVacancies)"
            self.experienceLabel.text = "\(jobDetail.yearOfExperience)"
            self.maxSalaryLabel.text = "\(jobDetail.maxSalary)"
            self.minSalaryLabel.text = "\(jobDetail.minSalary)"
            self.postingDate.text = "\(jobDetail.postingDate)"
            self.jobDescription.text = "\(jobDetail.jobDescription)"
            self.jobRequirement.text = "\(jobDetail.jobRequirement)"
            
            let postingDate = jobDetail.postingDate.replacingOccurrences(of: "/Date(", with: "")
            let postingDateArray = postingDate.replacingOccurrences(of: ")/", with: "").components(separatedBy: "+")
            
            if let str = postingDateArray.first, let myInteger = Int(str) {
                let myNumber = NSNumber(value:myInteger)
                let dateFormatter = DateFormatter.init()
                
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let date = self.dateFromMilliseconds(ms: myNumber)
                self.postingDate.text = dateFormatter.string(from: date as Date)
            }
            
            
            do {
                var html = self.jobDescription.text
                var doc: Document = try SwiftSoup.parse(html!)
                self.jobDescription.text = try doc.text()
                
                html = self.jobRequirement.text
                doc = try SwiftSoup.parse(html!)
                self.jobRequirement.text = try doc.text()
            } catch {
                print(error)
            }
        }
    }
    
    func dateFromMilliseconds(ms: NSNumber) -> NSDate {
        return NSDate(timeIntervalSince1970:Double(ms) / 1000.0)
    }
}
