//
//  JobListViewController.swift
//  ApplyJobs
//
//  Created by Tyson Cath on 21/02/18.
//  Copyright © 2018 Tyson. All rights reserved.
//

import UIKit

class JobListViewController: UIViewController {

    var jobList: Jobs? = nil
    @IBOutlet var listTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Jobs List"
        APIManager.getJobList { (jobs, errorVal) in
            self.jobList = jobs
            DispatchQueue.main.async {
                self.listTableView.reloadData()
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailController = segue.destination as? JobDetailViewController, let indexPath = sender as? IndexPath, let selectedModel = self.jobList?.jobs[indexPath.row] {
            detailController.selectedModel = selectedModel
        }
    }
}

extension JobListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = jobList?.jobs[indexPath.row] {
            self.performSegue(withIdentifier: "ShowJobDetailSegue", sender: indexPath)
        }
    }
}

extension JobListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobList?.jobs.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as? ListCell {
            if let model = jobList?.jobs[indexPath.row] {
                cell.jobTitleLabel.text = model.jobTitle
                cell.workerTypeLabel.text = model.workerType
                cell.vacanciesLabel.text = "\(model.numberOfVacancies)"
                cell.experienceLabel.text = "\(model.yearOfExperience)"
                cell.maxSalaryLabel.text = "\(model.maxSalary)"
                cell.minSalaryLabel.text = "\(model.minSalary)"
                return cell
            }
        }
            return UITableViewCell.init()
    }
}
