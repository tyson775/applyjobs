//
//  ProgressHUD.swift
//  ApplyJobs
//
//  Created by Tyson Vignesh on 22/02/18.
//  Copyright © 2018 Tyson. All rights reserved.
//

import Foundation
import SVProgressHUD

class ProgressHUD {
    static func show() {
        SVProgressHUD.show(withStatus: "Loading...")
    }
    
    static func hide() {
        SVProgressHUD.dismiss()
    }
}
