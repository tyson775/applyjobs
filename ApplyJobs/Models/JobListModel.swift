//
//  JobListModel.swift
//  ApplyJobs
//
//  Created by Tyson Cath on 22/02/18.
//  Copyright © 2018 Tyson. All rights reserved.
//

import Foundation
import Unbox

//Display fields
//    •    JobTitle
//    •    WorkerType
//    •    NumberOfVacancies
//    •    YearOfExperience
//    •    MaxSalary
//    •    MinSalary

struct Jobs {
    let jobs: [JobList]
}

extension Jobs: Unboxable {
    init(unboxer: Unboxer) throws {
        self.jobs = try unboxer.unbox(key: "Jobs")
    }
}
struct JobList {
    let jobID: String
    let jobTitle : String
    let workerType: String
    let numberOfVacancies: Int
    let yearOfExperience: Int
    let maxSalary: Int
    let minSalary: Int
}

extension JobList: Unboxable {
    init(unboxer: Unboxer) throws {
        self.jobID = try unboxer.unbox(key: "JobID")
        self.jobTitle = try unboxer.unbox(key: "JobTitle")
        self.workerType = try unboxer.unbox(key: "WorkerType")
        self.numberOfVacancies = try unboxer.unbox(key: "NumberOfVacancies")
        self.yearOfExperience = try unboxer.unbox(key: "YearOfExperience")
        self.maxSalary = try unboxer.unbox(key: "MaxSalary")
        self.minSalary = try unboxer.unbox(key: "MinSalary")
    }
}

//Display fields
//    •    JobTitle
//    •    WorkerType
//    •    NumberOfVacancies
//    •    YearOfExperience
//    •    MaxSalary
//    •    MinSalary
//    •    PostingDate
//    •    JobDescription
//    •    JobRequirement

struct JobDetails {
    let jobID: String
    let jobTitle : String
    let workerType: String
    let numberOfVacancies: Int
    let yearOfExperience: Int
    let maxSalary: Int
    let minSalary: Int
    let postingDate: String
    let jobDescription: String
    let jobRequirement: String
}

extension JobDetails: Unboxable {
    init(unboxer: Unboxer) throws {
        self.jobID = try unboxer.unbox(key: "JobID")
        self.jobTitle = try unboxer.unbox(key: "JobTitle")
        self.workerType = try unboxer.unbox(key: "WorkerType")
        self.numberOfVacancies = try unboxer.unbox(key: "NumberOfVacancies")
        self.yearOfExperience = try unboxer.unbox(key: "YearOfExperience")
        self.maxSalary = try unboxer.unbox(key: "MaxSalary")
        self.minSalary = try unboxer.unbox(key: "MinSalary")
        self.postingDate = try unboxer.unbox(key: "PostingDate")
        self.jobDescription = try unboxer.unbox(key: "JobDescription")
        self.jobRequirement = try unboxer.unbox(key: "JobRequirement")
    }
}
