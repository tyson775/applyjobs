//
//  APIManager.swift
//  ApplyJobs
//
//  Created by Tyson Cath on 21/02/18.
//  Copyright © 2018 Tyson. All rights reserved.
//

import UIKit
import Alamofire
import Unbox

let searchJobs = "http://52.163.56.202/FWMyApp_SIT/MobileServices.svc/searchjob"
typealias APIManagerJobListResponse  = (Jobs?, Error?) -> Void
typealias APIManagerJobDetailResponse  = (JobDetails?, Error?) -> Void
typealias APIManagerJobApplyResponse  = (String?, Error?) -> Void

class APIManager: NSObject {
    static func getJobList(completion: @escaping APIManagerJobListResponse) {
        let headers = [
            "content-type": "application/json"
        ]
        let parameters = [
            "keyword": "",
            "row": 10,
            "sortedBy": "",
            "start": 0,
            "tokenID": "6ea5152b-acc2-41e3-9b68-44bba8ca0024"
            ] as [String : Any]
        
        var postData = Data()
        
        do {
        postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            print(error)
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://52.163.56.202/FWMyApp_SIT/MobileServices.svc/searchjob")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            ProgressHUD.hide()
            if (error != nil) {
                print(error)
                completion(nil, error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode == 200, let dataData = data {
                    do {
                    let jobListArray: Jobs = try unbox(data: dataData)
                        completion(jobListArray, nil)
                    print(jobListArray)
                    } catch {
                        print(error)
                    }
                }
            }
        })
        ProgressHUD.show()
        dataTask.resume()
    }
    
    static func getIndividualJob(jobID id:String, completion: @escaping APIManagerJobDetailResponse) {
        let headers = [
            "content-type": "application/json"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://52.163.56.202/FWMyApp_SIT/MobileServices.svc/searchjob/details/\(id)?language=English")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            ProgressHUD.hide()
            if (error != nil) {
                print(error)
                completion(nil, error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode == 200, let dataData = data {
                    do {
                        let jobDetail: JobDetails = try unbox(data: dataData)
                        completion(jobDetail, nil)
                    } catch {
                        print(error)
                    }
                }
            }
        })
        ProgressHUD.show()
        dataTask.resume()
    }
    
    static func applyJob(withJobID jobID: String, image: UIImage, completion: @escaping APIManagerJobApplyResponse) {
        let headers = [
            "content-type": "application/json"
        ]
        //Now use image to create into NSData format
        let imageData: Data = UIImageJPEGRepresentation(image, 1)!

        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        print(strBase64)
        
        let parameters = [
            "JobID":jobID,
            "ImageData":[
                [
                    "ImageBase64":strBase64,
                    "ImageFileName":"img1.jpg"
                ]
            ],
            "tokenID": "6ea5152b-acc2-41e3-9b68-44bba8ca0024"
            ] as [String : Any]
        
        var postData = Data()
        
        do {
            postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            print(error)
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://52.163.56.202/FWMyApp_SIT/MobileServices.svc/applyjob")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            ProgressHUD.hide()
            if (error != nil) {
                print(error)
                completion(nil, error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode == 200, let dataData = data {
                    print(String.init(data: data!, encoding: String.Encoding.utf8)!)
                    do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: dataData, options: []) as? [String:AnyObject] {
                        completion(jsonResult["ResultText"] as! String, nil)
                    }
                    } catch {
                        
                    }
                }
            }
        })
        ProgressHUD.show()
        dataTask.resume()
    }
}
