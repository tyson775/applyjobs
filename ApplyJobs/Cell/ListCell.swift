//
//  ListCell.swift
//  ApplyJobs
//
//  Created by Tyson Vignesh on 22/02/18.
//  Copyright © 2018 Tyson. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {

    @IBOutlet var jobTitleLabel: UILabel!
    @IBOutlet var workerTypeLabel: UILabel!
    @IBOutlet var vacanciesLabel: UILabel!
    @IBOutlet var experienceLabel: UILabel!
    @IBOutlet var maxSalaryLabel: UILabel!
    @IBOutlet var minSalaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
